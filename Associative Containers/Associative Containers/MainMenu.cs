﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 5 - Associative Containers
//	File Name:		MainMenu.cs
//	Description:    Serves as the main form for the program. This form has all of the elements of the 
//                  state-capital matching game, including the list of capitals, the buttons to end game 
//                  and go to another question, etc...
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandon Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, April 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associative_Containers
{

    /// <summary>
    /// This is the Main Menu class. It controls the functions and processes of the State-Capital game
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class MainMenu : Form
    {
        SortedDict gameDictionary = new SortedDict(); //this will be the instance of the SortedDictionary we use
        Random rnd = new Random(); 
        int timeValue = 15; //This is the timer's value, set initially at 15 (seconds)
        int attempts = 0;   //Holds the number of attempts tried so far.
        int numCorrect = 0; //Holds the number of correct answers 
        string correctAnswer = ""; //This will hold whatever correct answer is for the turn
        double percentageCorrect;
       


        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            //the following code loads the Splash Screen
            Thread t = new Thread(new ThreadStart(SplashStart));
            t.Start();
            Thread.Sleep(5000);
            t.Abort();
            


            InitializeComponent();
            this.CenterToScreen();
            gameDictionary.loadFile();

            foreach (string str in gameDictionary.myDictionary.Values) //add all the Value entries into the Capitals List
                capitalListBox.Items.Add(str);

            capitalListBox.Sorted = true; //sort the list
            timeLeftBox.Text = timeValue.ToString(); //send the Value of timeValue to the timeLeftBox

            //set the Timer to have a larger (20) font
            timeLeftBox.Font = new Font(timeLeftBox.Font.Name, 20, timeLeftBox.Font.Style, timeLeftBox.Font.Unit);
            timeLeftBox.BackColor = System.Drawing.Color.LightGray; //set the background to gray
            timeLeftBox.ForeColor = System.Drawing.Color.Red;       //set the number to red coloring
            timeLeftBox.ReadOnly = true;                            //set the timer to be Read Only



        }


        /// <summary>
        /// Handles the Load event of the MainMenu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainMenu_Load(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Handles the TextChanged event of the AnswerBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void AnswerBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Handles the TextChanged event of the TimeLeftBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void TimeLeftBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Handles the TextChanged event of the NumCorrectBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void NumCorrectBox_TextChanged(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Gets a random Key from our SortedDictionary
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary.</param>
        /// <returns></returns>
        public TKey GetRandomElement<TKey, TValue>(SortedDictionary<TKey, TValue> dict)
        {
            Random randGen = new Random();
            int randIndex = randGen.Next(dict.Values.Count);
            int i = 0;
            foreach (TKey key in dict.Keys)
            {
                if (i++ == randIndex)
                {
                    correctAnswer = gameDictionary.myDictionary[key.ToString()];
                    return key;
                }
            }
            

            return default(TKey);
        }


        /// <summary>
        /// Handles the Tick event of the timeToAnswer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void timeToAnswer_Tick(object sender, EventArgs e)
        {
            if (timeValue > 0) //so long as there is time left...
            {
                nxtButton.Enabled = false;
                timeValue--; //decrease the timeValue by one per tick
                timeLeftBox.Text = timeValue.ToString();
                if (timeValue == 0) //if there is no time left...do below
                {
                    timeUpLabel.Visible = true;
                    nxtButton.Enabled = true;
                    attempts++;
                    capitalListBox.Enabled = false;

                }
            }
        }

        /// <summary>
        /// Disables the buttons.
        /// </summary>
        public void disableButtons()
        {

        }

        /// <summary>
        /// Handles the Click event of the nxtButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void nxtButton_Click(object sender, EventArgs e)
        {
            incorrectLabel.Visible = false;
            crctLabel.Visible = false;
            timeUpLabel.Visible = false;

            timeLeftBox.Text = 15.ToString();
            timeValue = 15;                     //reset the timer
            stateBox.Text = GetRandomElement<string, string>(gameDictionary.myDictionary);  //put another random State in the box
            capitalListBox.Enabled = true;

        }



        /// <summary>
        /// Handles the SelectedIndexChanged event of the capitalListBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void capitalListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (capitalListBox.SelectedItem.Equals(correctAnswer)) //if the correct answer is chosen...
            {
                crctLabel.Visible = true;
                nxtButton.Enabled = true;
                timeLeftBox.Text = 0.ToString();
                timeValue = 0;
                numCorrect++;
                attempts++;
                numCorrectBox.Text = numCorrect.ToString();
                attemptsBox.Text = attempts.ToString();
                capitalListBox.Enabled = false;

            }
            else //if the answer is wrong...
            {
                incorrectLabel.Visible = true;
                nxtButton.Enabled = true;
                timeLeftBox.Text = 0.ToString();
                timeValue = 0;
                attempts++;
                attemptsBox.Text = attempts.ToString();
                capitalListBox.Enabled = false;


            }


        }

        /// <summary>
        /// Presents a loading screen to the user
        /// </summary>
        public void SplashStart()
        {
            Application.Run(new SplashForm());
        }



        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains the event data.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {

            e.Cancel = true; //necessary to prevent forced closure of form
            percentageCorrect = (numCorrect / (double)attempts * 100); //sets up the percentage of correct answers
            if (percentageCorrect.Equals(double.NaN)) //if "not a number" is the percent correct, catch it.
                percentageCorrect = 0; //set it to zero
            closingForm newClosingForm = new closingForm(); //create an instance of the closing form
            newClosingForm.CustomMessage("Thank you for playing the State-Capital Matching Game!\nAt this time, you got " + Math.Round(percentageCorrect, 2) + "% right out of " + attempts + "! \nAre you sure you want to close?");
            newClosingForm.StartPosition = FormStartPosition.CenterParent;
            newClosingForm.ShowDialog(this);
          
        }

        
        /// <summary>
        /// Handles the Click event of the endButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void endButton_Click(object sender, EventArgs e)
        {
            
            percentageCorrect = (numCorrect / (double)attempts * 100); //sets up the percentage of correct answers
            if (percentageCorrect.Equals(double.NaN)) //if "not a number" is the percent correct, catch it.
                percentageCorrect = 0; //set it to zero
            closingForm newClosingForm = new closingForm(); //create an instance of the closing form
            newClosingForm.CustomMessage("Thank you for playing the State-Capital Matching Game!\nAt this time, you got " + Math.Round(percentageCorrect, 2) + "% right out of " + attempts + "! \nAre you sure you want to close?");
            newClosingForm.StartPosition = FormStartPosition.CenterParent;
            newClosingForm.ShowDialog(this);

        }

        /// <summary>
        /// Handles the Click event of the beginButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void beginButton_Click(object sender, EventArgs e)
        {
            beginButton.Visible = false;
            beginButton.Enabled = false;
            capitalListBox.Enabled = true;
            stateBox.Text = GetRandomElement<string, string>(gameDictionary.myDictionary);//get a random State from the Dictionary
            timeToAnswer.Start();                    //start the Timer for the game


        }
    }
}
