﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 5 - Associative Containers
//	File Name:		SortedDict.cs
//	Description:    This class manages a SortedDictionary to be used by the MainMenu. It will manage a list
//                  of States and their Capitals.
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandon Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, April 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associative_Containers
{

    /// <summary>
    /// This class manages a SortedDictionary to be used by the MainMenu. It will manage a list
    ///  of States and their Capitals.
    /// </summary>
    class SortedDict
    {

        //create a new SortedDictionary named myDictionary
        public SortedDictionary<string, string> myDictionary = new SortedDictionary<string, string>();



        /// <summary>
        /// Loads the file that will read in a list of States and Capitals
        /// </summary>
        public void loadFile()
        {

            string sFileName = "";
            bool fileLoaded;
            string statesCapitalsText = "";
            string stateInfo = "";
            Regex commaSpaceRgx = new Regex(", ");
            

            
                try
                {
                    sFileName = System.IO.Path.GetFullPath(@"..\..\Text Files\StatesAndCapitals.txt"); //set the chosen file path to sFileName

                    fileLoaded = true; //make a note that a file has been loaded

                }
                catch (Exception x)
                {
                    MessageBox.Show("File was not loaded"); //if there is a problem loading the file, inform the user

                }

                try
                {
                    // Create an instance of StreamReader to read from a file.
                    // The using statement also closes the StreamReader.

                    using (StreamReader sr = new StreamReader(sFileName))
                    {
                        while (sr.Peek() >= 0)
                        {
                            string[] str = Regex.Split(sr.ReadLine(),", ");
                            myDictionary.Add(str[1], str[0]);
                        }

                        sr.Close();
                    }

                }
                catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }
        }
    }

