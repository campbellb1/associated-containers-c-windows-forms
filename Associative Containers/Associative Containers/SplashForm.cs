﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 5 - Associative Containers
//	File Name:		SplashForm.cs
//	Description:    Handles the "SplashScreen" and the speed at which the progress bar fills
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandon Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, April 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associative_Containers
{

    /// <summary>
    /// Handles the "SplashScreen" and the speed at which the progress bar fills
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class SplashForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SplashForm"/> class.
        /// </summary>
        public SplashForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }



        /// <summary>
        /// Handles the Tick event of the splashTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void splashTimer_Tick(object sender, EventArgs e)
        {
            
            loadBar.Increment(1);
            if(loadBar.Value == 100)
            {
                splashTimer.Stop();
            }
        }



        /// <summary>
        /// Handles the Click event of the splashLoadBar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void splashLoadBar_Click(object sender, EventArgs e)
        {

        }
    }
}
