﻿namespace Associative_Containers
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.stateLabel = new System.Windows.Forms.Label();
            this.stateBox = new System.Windows.Forms.TextBox();
            this.timeLeftLabel = new System.Windows.Forms.Label();
            this.timeLeftBox = new System.Windows.Forms.TextBox();
            this.capitalLabel = new System.Windows.Forms.Label();
            this.capitalListBox = new System.Windows.Forms.ListBox();
            this.attemptsLabel = new System.Windows.Forms.Label();
            this.correctLabel = new System.Windows.Forms.Label();
            this.attemptsBox = new System.Windows.Forms.TextBox();
            this.numCorrectBox = new System.Windows.Forms.TextBox();
            this.nxtButton = new System.Windows.Forms.Button();
            this.endButton = new System.Windows.Forms.Button();
            this.timeToAnswer = new System.Windows.Forms.Timer(this.components);
            this.timeUpLabel = new System.Windows.Forms.Label();
            this.crctLabel = new System.Windows.Forms.Label();
            this.incorrectLabel = new System.Windows.Forms.Label();
            this.beginButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Location = new System.Drawing.Point(16, 52);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(139, 13);
            this.stateLabel.TabIndex = 0;
            this.stateLabel.Text = "What is the state capital of: ";
            // 
            // stateBox
            // 
            this.stateBox.Location = new System.Drawing.Point(161, 49);
            this.stateBox.Name = "stateBox";
            this.stateBox.ReadOnly = true;
            this.stateBox.Size = new System.Drawing.Size(100, 20);
            this.stateBox.TabIndex = 1;
            this.stateBox.TextChanged += new System.EventHandler(this.AnswerBox_TextChanged);
            // 
            // timeLeftLabel
            // 
            this.timeLeftLabel.AutoSize = true;
            this.timeLeftLabel.Location = new System.Drawing.Point(53, 103);
            this.timeLeftLabel.Name = "timeLeftLabel";
            this.timeLeftLabel.Size = new System.Drawing.Size(102, 13);
            this.timeLeftLabel.TabIndex = 2;
            this.timeLeftLabel.Text = "Time left to answer: ";
            // 
            // timeLeftBox
            // 
            this.timeLeftBox.Location = new System.Drawing.Point(161, 96);
            this.timeLeftBox.Name = "timeLeftBox";
            this.timeLeftBox.Size = new System.Drawing.Size(50, 20);
            this.timeLeftBox.TabIndex = 3;
            this.timeLeftBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.timeLeftBox.TextChanged += new System.EventHandler(this.TimeLeftBox_TextChanged);
            // 
            // capitalLabel
            // 
            this.capitalLabel.AutoSize = true;
            this.capitalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capitalLabel.Location = new System.Drawing.Point(329, 14);
            this.capitalLabel.Name = "capitalLabel";
            this.capitalLabel.Size = new System.Drawing.Size(152, 20);
            this.capitalLabel.TabIndex = 4;
            this.capitalLabel.Text = "Select the Capital";
            // 
            // capitalListBox
            // 
            this.capitalListBox.Enabled = false;
            this.capitalListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capitalListBox.FormattingEnabled = true;
            this.capitalListBox.ItemHeight = 20;
            this.capitalListBox.Location = new System.Drawing.Point(324, 37);
            this.capitalListBox.Name = "capitalListBox";
            this.capitalListBox.ScrollAlwaysVisible = true;
            this.capitalListBox.Size = new System.Drawing.Size(175, 324);
            this.capitalListBox.TabIndex = 5;
            this.capitalListBox.SelectedIndexChanged += new System.EventHandler(this.capitalListBox_SelectedIndexChanged);
            // 
            // attemptsLabel
            // 
            this.attemptsLabel.AutoSize = true;
            this.attemptsLabel.Location = new System.Drawing.Point(51, 250);
            this.attemptsLabel.Name = "attemptsLabel";
            this.attemptsLabel.Size = new System.Drawing.Size(48, 13);
            this.attemptsLabel.TabIndex = 6;
            this.attemptsLabel.Text = "Attempts";
            // 
            // correctLabel
            // 
            this.correctLabel.AutoSize = true;
            this.correctLabel.Location = new System.Drawing.Point(195, 250);
            this.correctLabel.Name = "correctLabel";
            this.correctLabel.Size = new System.Drawing.Size(41, 13);
            this.correctLabel.TabIndex = 7;
            this.correctLabel.Text = "Correct";
            // 
            // attemptsBox
            // 
            this.attemptsBox.Location = new System.Drawing.Point(38, 266);
            this.attemptsBox.Name = "attemptsBox";
            this.attemptsBox.ReadOnly = true;
            this.attemptsBox.Size = new System.Drawing.Size(78, 20);
            this.attemptsBox.TabIndex = 8;
            this.attemptsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numCorrectBox
            // 
            this.numCorrectBox.Location = new System.Drawing.Point(180, 266);
            this.numCorrectBox.Name = "numCorrectBox";
            this.numCorrectBox.ReadOnly = true;
            this.numCorrectBox.Size = new System.Drawing.Size(81, 20);
            this.numCorrectBox.TabIndex = 9;
            this.numCorrectBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numCorrectBox.TextChanged += new System.EventHandler(this.NumCorrectBox_TextChanged);
            // 
            // nxtButton
            // 
            this.nxtButton.Enabled = false;
            this.nxtButton.Location = new System.Drawing.Point(38, 319);
            this.nxtButton.Name = "nxtButton";
            this.nxtButton.Size = new System.Drawing.Size(87, 23);
            this.nxtButton.TabIndex = 10;
            this.nxtButton.Text = "Next Question";
            this.nxtButton.UseVisualStyleBackColor = true;
            this.nxtButton.Click += new System.EventHandler(this.nxtButton_Click);
            // 
            // endButton
            // 
            this.endButton.Location = new System.Drawing.Point(174, 319);
            this.endButton.Name = "endButton";
            this.endButton.Size = new System.Drawing.Size(87, 23);
            this.endButton.TabIndex = 11;
            this.endButton.Text = "End Game";
            this.endButton.UseVisualStyleBackColor = true;
            this.endButton.Click += new System.EventHandler(this.endButton_Click);
            // 
            // timeToAnswer
            // 
            this.timeToAnswer.Interval = 1000;
            this.timeToAnswer.Tick += new System.EventHandler(this.timeToAnswer_Tick);
            // 
            // timeUpLabel
            // 
            this.timeUpLabel.AutoSize = true;
            this.timeUpLabel.Font = new System.Drawing.Font("Ravie", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeUpLabel.ForeColor = System.Drawing.Color.Red;
            this.timeUpLabel.Location = new System.Drawing.Point(45, 172);
            this.timeUpLabel.Name = "timeUpLabel";
            this.timeUpLabel.Size = new System.Drawing.Size(197, 36);
            this.timeUpLabel.TabIndex = 12;
            this.timeUpLabel.Text = "Time\'s Up!";
            this.timeUpLabel.Visible = false;
            // 
            // crctLabel
            // 
            this.crctLabel.AutoSize = true;
            this.crctLabel.Font = new System.Drawing.Font("Ravie", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crctLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.crctLabel.Location = new System.Drawing.Point(60, 172);
            this.crctLabel.Name = "crctLabel";
            this.crctLabel.Size = new System.Drawing.Size(168, 36);
            this.crctLabel.TabIndex = 13;
            this.crctLabel.Text = "Correct!";
            this.crctLabel.Visible = false;
            // 
            // incorrectLabel
            // 
            this.incorrectLabel.AutoSize = true;
            this.incorrectLabel.Font = new System.Drawing.Font("Ravie", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incorrectLabel.ForeColor = System.Drawing.Color.Red;
            this.incorrectLabel.Location = new System.Drawing.Point(50, 172);
            this.incorrectLabel.Name = "incorrectLabel";
            this.incorrectLabel.Size = new System.Drawing.Size(228, 36);
            this.incorrectLabel.TabIndex = 14;
            this.incorrectLabel.Text = "Incorrect...";
            this.incorrectLabel.Visible = false;
            // 
            // beginButton
            // 
            this.beginButton.BackColor = System.Drawing.Color.LightGreen;
            this.beginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.beginButton.Location = new System.Drawing.Point(50, 93);
            this.beginButton.Name = "beginButton";
            this.beginButton.Size = new System.Drawing.Size(178, 43);
            this.beginButton.TabIndex = 15;
            this.beginButton.Text = "BEGIN";
            this.beginButton.UseVisualStyleBackColor = false;
            this.beginButton.Click += new System.EventHandler(this.beginButton_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 401);
            this.Controls.Add(this.beginButton);
            this.Controls.Add(this.incorrectLabel);
            this.Controls.Add(this.crctLabel);
            this.Controls.Add(this.timeUpLabel);
            this.Controls.Add(this.endButton);
            this.Controls.Add(this.nxtButton);
            this.Controls.Add(this.numCorrectBox);
            this.Controls.Add(this.attemptsBox);
            this.Controls.Add(this.correctLabel);
            this.Controls.Add(this.attemptsLabel);
            this.Controls.Add(this.capitalListBox);
            this.Controls.Add(this.capitalLabel);
            this.Controls.Add(this.timeLeftBox);
            this.Controls.Add(this.timeLeftLabel);
            this.Controls.Add(this.stateBox);
            this.Controls.Add(this.stateLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu";
            this.Text = "State Capital Matching Game";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.TextBox stateBox;
        private System.Windows.Forms.Label timeLeftLabel;
        private System.Windows.Forms.TextBox timeLeftBox;
        private System.Windows.Forms.Label capitalLabel;
        private System.Windows.Forms.ListBox capitalListBox;
        private System.Windows.Forms.Label attemptsLabel;
        private System.Windows.Forms.Label correctLabel;
        private System.Windows.Forms.TextBox attemptsBox;
        private System.Windows.Forms.TextBox numCorrectBox;
        private System.Windows.Forms.Button nxtButton;
        private System.Windows.Forms.Button endButton;
        private System.Windows.Forms.Timer timeToAnswer;
        private System.Windows.Forms.Label timeUpLabel;
        private System.Windows.Forms.Label crctLabel;
        private System.Windows.Forms.Label incorrectLabel;
        private System.Windows.Forms.Button beginButton;
    }
}

