﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 5 - Associative Containers
//	File Name:		ClosingForm.cs
//	Description:    Serves as the closing message for the program
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandon Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, April 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associative_Containers
{

    /// <summary>
    /// Serves as the closing message for the program
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class closingForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="closingForm"/> class.
        /// </summary>
        public closingForm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Handles the Click event of the YesButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void YesButton_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }


        /// <summary>
        /// Adds a custom closing message
        /// </summary>
        /// <param name="message">The message.</param>
        public void CustomMessage(string message)
        {
            closingMessage.Text = message;
        }


        /// <summary>
        /// Handles the Click event of the noButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void noButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
