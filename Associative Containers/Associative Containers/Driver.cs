﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Project 5 - Associative Containers
//	File Name:		Driver.cs
//	Description:    This class runs the main menu and gets the 'whole program' started
//	Course:			CSCI 2210-001 - Data Structures
//	Author:			Brandon Campbell, campbellb1@goldmail.etsu.edu
//	Created:		Thursday, April 21, 2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Associative_Containers
{

    /// <summary>
    /// This class runs the main menu and gets the 'whole program' started
    /// </summary>
    static class Driver
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu());
        }
    }
}
